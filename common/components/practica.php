<?php

namespace common\components;

use yii\base\Event;
use yii\base\Component;
use yii\helpers\Html;

class Practica extends Component{

	/**
     * Constante, antes de mostrar.
     *
     */
    const EVENT_BEFORE_MOSTRAR = 'beforMostrar';
    public $mensaje;

    /**
     * Valor por default
     */
    public function init(){
		
		parent::init();
		$this->mensaje= 'Default';
	}

	/**
     * Devuelve mensaje
     * @return string
     */
    public function mostrar($mensaje = null){
		
		if($mensaje!=null){
			$this->mensaje= $mensaje;
		}

		if (!$this->beforMostrar()) {
            return '';
        }
        
        $response = $this->mensaje;
        $this->afterMostrar();

        return $response;
	}
    
    /**
     * Dispara el evento antes de ejecutar
     * funcion mostrar.
     *     
     */

   public function beforMostrar()
    {
         $event = new Event();
         $this->trigger(self::EVENT_BEFORE_MOSTRAR, $event);
         echo "Antes";
         return true;
    }
    
    /**
     * Dispara evento después de ejecutar 
     * funcion mostrar.
     */
    public function afterMostrar()
   {
        $event = new Event();
        $this->trigger(self::EVENT_BEFORE_MOSTRAR, $event);
        echo "despues";
        return true;
    }

}


