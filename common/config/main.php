<?php
return [
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    'components' => [
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'practica' => [
            'class' => 'common\components\practica',
            'mensaje' => 'Probando...'
        ],
    ],
];
